<?php
require_once('./models/Unidad.php');
require_once('./models/Caballero.php');
require_once('./models/Arquero.php');
require_once('./models/Piquero.php');
require_once('./models/Ejercito.php');
require_once('./models/Civilizacion.php');

function inicializarEjercitosCivilizacion($civilizacion, $cant_piqueros, $cant_arqueros
, $cant_caballeros) {
    $ejercito = new Ejercito();
    for ($i = 0; $i < $cant_piqueros; $i++) {
        $ejercito->agregarUnidad(new Piquero());
    }
    for ($i = 0; $i < $cant_arqueros; $i++) {
        $ejercito->agregarUnidad(new Arquero());
    }

    for ($i = 0; $i < $cant_caballeros; $i++) {
        $ejercito->agregarUnidad(new Caballero());
    }

    $civilizacion->agregarEjercito($ejercito);
}
#Creo Civilizaciones
$chinos = new Civilizacion("Chinos");
$ingleses = new Civilizacion("Ingleses");
$bizantinos = new Civilizacion("Bizantinos");

#Inicializo ejercitos
inicializarEjercitosCivilizacion($chinos, 2, 25, 2);
inicializarEjercitosCivilizacion($ingleses, 1, 26, 2);
inicializarEjercitosCivilizacion($bizantinos, 5, 8, 15);

//Entreno el segundo piquero de la civilización China
$piquero = $chinos->ejercitos[0]->unidades[1];
//print_r($piquero);
//$chinos->ejercitos[0]->entrenar($piquero);
//print_r($piquero);
print_r($chinos->ejercitos[0]->monedas_oro);
echo "\n\n\n";
//Se transforma el segundo piquero a Arquero
$chinos->ejercitos[0]->transformar($piquero);
//print_r($chinos->ejercitos[0]->unidades);
print_r($chinos->ejercitos[0]->monedas_oro);
//Intento transformar primer Caballero
$caballero = $chinos->ejercitos[0]->unidades[27];
print_r("\n".get_class($caballero));
//$chinos->ejercitos[0]->transformar($caballero);
$chinos->ejercitos[0]->transformar($caballero);
//print_r($chinos->ejercitos[0]->unidades);
echo "\n\n\n";
//Enfrentar Ejercito de China contra Ejercito de Ingleses
echo "Ejercito Chino\n";
echo "Cantidad de Unidades: " . count($chinos->ejercitos[0]->unidades) . "\n";
echo "Cantidad de Monedas de Oro: " . $chinos->ejercitos[0]->monedas_oro . "\n";
echo "Cantidad de Puntos: " . $chinos->ejercitos[0]->contarPuntos() . "\n\n\n";
echo "Ejercito Inglés\n";
echo "Cantidad de Unidades: " . count($ingleses->ejercitos[0]->unidades) . "\n";
echo "Cantidad de Monedas de Oro: " . $ingleses->ejercitos[0]->monedas_oro . "\n";
echo "Cantidad de Puntos: " . $ingleses->ejercitos[0]->contarPuntos() . "\n\n\n";
$chinos->ejercitos[0]->atacar($ingleses->ejercitos[0]);
print_r($chinos->ejercitos[0]->unidades);
echo "Ejercito Chino\n";
echo "Cantidad de Unidades: " . count($chinos->ejercitos[0]->unidades) . "\n";
echo "Cantidad de Monedas de Oro: " . $chinos->ejercitos[0]->monedas_oro . "\n";
echo "Cantidad de Puntos: " . $chinos->ejercitos[0]->contarPuntos() . "\n\n\n";
echo "Ejercito Inglés\n";
echo "Cantidad de Unidades: " . count($ingleses->ejercitos[0]->unidades) . "\n";
echo "Cantidad de Monedas de Oro: " . $ingleses->ejercitos[0]->monedas_oro . "\n";
echo "Cantidad de Puntos: " . $ingleses->ejercitos[0]->contarPuntos() . "\n\n\n";