<?php

class Caballero extends Unidad {

    public function __construct() {
        $this->puntos_fuerza = 20;
        $this->puntos_entrenamiento = 10;
        $this->costo_entrenamiento = 30;
        $this->costo_transformacion = 0; 
    }

    public function transformar() {
        return $this;
    }
}