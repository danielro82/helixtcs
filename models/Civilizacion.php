<?php

class Civilizacion {

    public function __construct($nombre) {
        $this->ejercitos = [];
        $this->nombre = $nombre;
    }

    public function agregarEjercito($ejercito) {
        $this->ejercitos[] = $ejercito;
    }
}