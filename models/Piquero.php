<?php

require_once('Arquero.php');

class Piquero extends Unidad {

    public function __construct() {
        $this->puntos_fuerza = 5;
        $this->puntos_entrenamiento = 3;
        $this->costo_entrenamiento = 10; 
        $this->costo_transformacion = 30;
    }

    public function transformar() {
        return new Arquero();
    }
}