<?php

class Arquero extends Unidad {

    public function __construct() {
        $this->puntos_fuerza = 10;
        $this->puntos_entrenamiento = 7;
        $this->costo_entrenamiento = 20; 
        $this->costo_transformacion = 40;
    }

    public function transformar() {
        return new Caballero();
    }
}