<?php

class Ejercito {
    
    public function __construct() { 
        $this->unidades = [];
        $this->monedas_oro = 1000;
    }

    public function contarPuntos() {
        $puntos = 0;
        foreach ($this->unidades as $unidad) {
            $puntos += $unidad->puntos_fuerza;
        }
        return $puntos;
    }

    public function agregarMonedasDeOro($monedas) {
        $this->monedas_oro += $monedas;
    }

    public function atacar($ejercito_rival) {
        $puntosPropios = $this->contarPuntos();
        $puntosRivales = $ejercito_rival->contarPuntos();


        if ($puntosPropios > $puntosRivales) {
            $this->agregarMonedasDeOro(100);
            $ejercito_rival->eliminarMejorUnidad();
            $ejercito_rival->eliminarMejorUnidad();
        } else if ($puntosPropios < $puntosRivales) {
            $ejercito_rival->agregarMonedasDeOro(100);
            $this->eliminarMejorUnidad();
            $this->eliminarMejorUnidad();
        } else {
            $this->quitarUnidad(array_rand($this->unidades));
            $ejercito_rival->quitarUnidad(array_rand($ejercito_rival->unidades));
        }
    }

    public function eliminarMejorUnidad() {
        $mejorUnidad = reset($this->unidades);
        $mejorUnidadKey = key($this->unidades);
        foreach ($this->unidades as $key=>$value) {
            if ($mejorUnidad->puntos_fuerza < $this->unidades[$key]->puntos_fuerza) {
                $mejorUnidad = $this->unidades[$key];
                $mejorUnidadKey = $key;
            }
        }
        $this->quitarUnidad($mejorUnidadKey);
    }

    public function agregarUnidad($unidad){
        $this->unidades[] = $unidad;
    }

    public function quitarUnidad($unidadKey){
        foreach ($this->unidades as $key=>$value) {
            if ($key === $unidadKey) {
                unset($this->unidades[$key]);
                break;
            }
        }
        
    }

    public function quitarUnidadByValue($unidad){
        foreach ($this->unidades as $key=>$value) {
            if ($value === $unidad) {
                unset($this->unidades[$key]);
                break;
            }
        }        
    }

    public function entrenar($unidad){
        $this->monedas_oro -= $unidad->costo_entrenamiento;
        $unidad->entrenar();
    }

    public function transformar($unidad){
        $this->monedas_oro -= $unidad->costo_transformacion;
        $this->agregarUnidad($unidad->transformar());
        $this->quitarUnidadByValue($unidad);
    }
}
